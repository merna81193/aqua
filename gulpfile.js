const gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  svgstore = require('gulp-svgstore'),
  svgmin = require('gulp-svgmin'),
  path = require('path'),
  fileinclude = require('gulp-file-include'),
  concat = require('gulp-concat'),
  cheerio = require('gulp-cheerio'),
  sourcemaps = require('gulp-sourcemaps');


gulp.task('browser-sync', function (done) {
  browserSync.init({
    server: {
      baseDir: "./",
      directory: true
    }
  });
  done();
});
gulp.task('sass', function () {
  return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
    .pipe(sass({ includePaths: ['./node_modules'] }))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('src/dist/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task('svgstore', function () {
  return gulp
    .src('./src/assets/svg/*.svg')
    .pipe(cheerio({
      run: function ($) {
        $('[fill]').removeAttr('fill');
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(gulp.dest('src/dist/svg'));
});
gulp.task('fileinclude', function () {
  var fileincludeArr = [
    './src/html/components/pages/**',
  ];
  return gulp.src(fileincludeArr)
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
    }))
    .pipe(gulp.dest('./src/html'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('javascript', function () {
  return gulp.src([
    'node_modules/owl.carousel/dist/owl.carousel.min.js',
    'node_modules/mmenu-js/dist/mmenu.js',
    'src/js/app.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('main.bundle.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('src/dist/js'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task('watch', function () {
  gulp.watch(['./src/scss/**', './src/scss/**.scss'], gulp.series("sass"));
  gulp.watch(['./src/html/components/**'], gulp.series("fileinclude")).on('change', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  gulp.watch(['./src/js/**.js'], gulp.series("javascript")).on('change', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  gulp.watch([
    './src/html/**'
  ]).on('change', browserSync.reload);
  gulp.watch([
    './src/js/**.js'
  ]).on('change', browserSync.reload);
});



gulp.task('default', gulp.series('browser-sync', 'sass', 'svgstore', 'fileinclude', 'javascript', 'watch'));
