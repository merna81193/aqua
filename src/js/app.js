function homeCarousel(e, options) {
    console.log("halloooooooooooooogfd oooooooooooo")
    let $dotsContainer = $(e).find('.carousel-dots-wrapper'),
        $navContainer = $(e).find('.carousel-navigation-wrapper');
    $carouselEle = $(e).find('.owl-carousel');
    options.dotsContainer = $dotsContainer || false;
    options.navContainer = $navContainer || false;
    options.navText = [`<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
  </svg>`, `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
</svg>`];
    $carouselEle.owlCarousel(options);
}

$(document).ready(function () {
    homeCarousel(".carousel-wrapper",
        {
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: '3000',
            responsiveClass: true,
            stagePadding: 0,

        }
    )
    homeCarousel(".banner-carousel", {
        items: 1,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: '3000',
        responsiveClass: true,
        stagePadding: 0,

    })
    homeCarousel(".triple-carousel-item", {
        items: 3,
        loop: true,
        margin: 0,
        responsiveClass: true,
        stagePadding: 0,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            // breakpoint from 768 up
            768: {
                items: 2,
            },
            // breakpoint from 991 up
            1200: {
                items: 3
            }
        },

    });
    homeCarousel(".single-carousel-item", {
        items: 1,
        loop: true,
        margin: 0,
        responsiveClass: true,
        stagePadding: 0,

    });
    homeCarousel(".single-carousel-item-auto-play", {
        items: 1,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: '3000',
        responsiveClass: true,
        stagePadding: 0,

    });
   
});
